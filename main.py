import requests as rq
import json

from time import sleep
from datetime import datetime

from emailPolling import EmailPolling
from ircPolling import IRCPolling

if __name__ == '__main__':
  nlp_addr = 'http://msfloss-nlp.herokuapp.com/'

  emailPolling = EmailPolling('https://limitless-taiga-45801.herokuapp.com/api/v1', nlp_addr)
  ircPolling = IRCPolling('https://limitless-taiga-45801.herokuapp.com/api/v1', nlp_addr)

  services = [
    ('email', emailPolling),
    ('irc', ircPolling),
  ]

  for service in services:
    service[1].requestData()
    print(service[0] + 'polling finished')
