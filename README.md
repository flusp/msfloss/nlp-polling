# NLP Polling

This script is built to poll data from other services in the **MsFLOSS** system and format it to send it to the NLP service API. This exists to avoid coupling the NLP service
tightly into the other services.

The only thing, you need to do in order to run this script is:
```bash
python3 main.py
```

This script pulls from both the email and commit services, and sent the formatted data to the NLP service.

### Observations
Since the `Issue` service is down, I removed the polling from that service.