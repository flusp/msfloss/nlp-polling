import requests as rq
import json
from datetime import datetime

class IRCPolling:
  def __init__(self, url, nlp_addr):
    self.url = url
    self.date = datetime.fromtimestamp(0).isoformat()
    self.nlp_addr = nlp_addr
    self.headers = {
      'Content-Type': 'application/json'
    }
  
  def requestData(self):
    conv_uri = self.nlp_addr + 'conversation/'


    messages = rq.get(self.url + '/messages').json()
    self.date = datetime.now().isoformat()

    conversations = rq.get(conv_uri).json()['conversations']

    conv_list = []
    conv_tuple_list = []

    # The identifier of a conversation in a IRC chat, is the combination of a channel and
    # a server, there's why, we are using tuples for comparison

    for conv in conversations:
      if 'server' in conv['context'].keys() and 'channel' in conv['context'].keys():
        context_tuple = (conv['context']['channel'], conv['context']['server'])
        if context_tuple not in conv_tuple_list:
          conv_tuple_list.append(context_tuple)
          conv_list.append(conv)

    for message in messages:
      msg_tuple = (message['channel'], message['server'])
      if msg_tuple not in conv_tuple_list:
        conv = {
          'source': 'IRC',
          'context': {
            'channel': message['channel'],
            'server': message['server']
          }
        }

        conv['id'] = rq.post(url=conv_uri, json=conv).json()['id']
        conv_list.append(conv)
        conv_tuple_list.append(msg_tuple)

      conv_id = ''
      for conv in conv_list:
        if (conv['context']['channel'], conv['context']['server']) == msg_tuple:
          conv_id = conv['id']
      
      event_uri = self.nlp_addr + 'event/' + conv_id
      event = {
        'user': message['nick'],
        'timestamp': message['time'],
        'type': 'MSG',
        'content': message['message']
      }
      rq.post(event_uri, json=event)
