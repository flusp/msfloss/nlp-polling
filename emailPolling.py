import requests as rq
import json
from datetime import datetime

class EmailPolling:
  def __init__(self, url, nlp_addr):
    self.url = url
    self.date = datetime.fromtimestamp(0).isoformat()
    self.nlp_addr = nlp_addr
    self.headers = {
      'Content-Type': 'application/json'
    }
  
  def requestData(self):
    conv_uri = self.nlp_addr + 'conversation/'


    emails = rq.get(self.url + '/emails/query?start_date=' + self.date).json()
    self.date = datetime.now().isoformat()

    conversations = rq.get(conv_uri).json()['conversations']

    conv_list = []
    conv_name_list = []

    for conv in conversations:
      if 'list-name' in conv['context'].keys() and conv['context']['list-name'] not in conv_name_list:
        conv_name_list.append(conv['context']['list-name'])
        conv_list.append(conv)

    for email in emails:
      if email['recipient'] not in conv_name_list and email['recipient'] != '':
        conv = {
          'source': 'MAIL',
          'context': {
            'list-name': email['recipient']
          }
        }

        resp = rq.post(url=conv_uri, json=conv).json()
        print(resp)
        conv['id'] = resp['id']
        conv_list.append(conv)
        conv_name_list.append(email['recipient'])

      conv_id = ''
      for conv in conv_list:
        if conv['context']['list-name'] == email['recipient']:
          conv_id = conv['id']

      
      event_uri = self.nlp_addr + 'event/' + conv_id
      event = {
        'user': email['sender'],
        'timestamp': email['date'],
        'type': 'MSG',
        'content': email['message']
      }
      rq.post(event_uri, json=event)
